#!/bin/bash
read -p "Select a voice: " tiktokvoice
echo "What should I say?"
read -e tiktoktext
tiktokurl=$(echo $tiktoktext |jq -sRr @uri)
curl -X post "https://api16-normal-useast5.us.tiktokv.com/media/api/text/speech/invoke/?text_speaker=$tiktokvoice&req_text=$tiktokurl" \
|jq .data.v_str \
|base64 -di > /tmp/tiktokresult.mp3
ffplay -loglevel error -stats -nodisp -autoexit /tmp/tiktokresult.mp3
rm /tmp/tiktokresult.mp3
