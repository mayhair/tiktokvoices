Simple script where you can make the infamous TikTok voices say
anything without opening that dang app.

Dependencies:
bash
jq
curl
base64
ffplay

How to use:

Run voices.sh.

When it tells you to select a voice, refer to voices.txt and enter the
voice you would like.

Press enter and it will ask you "What should I say?" Answer that with
your keyboard.

Press enter again and let the script do its magic.

Please note that I have little prior experience in shell scripting. I
could only do this with random Stack Overflow answers. The script may
be a bit messy as a result.

Credit to:
@scanlime@twitter.com for sharing the TikTok API request.
(https://twitter.com/scanlime/status/1512278082685571073)
syslenok Discord bot (on The Flying Tech Server) for inspiring this
script.
